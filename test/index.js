
function index(){
    console.log("doing stuff man");
    //xmlRequest("30May19", 8, "1200", "1200");
    //var aph_price = xmlRequest();


    const response = xmlRequestPromise();

    response.then(function(result) {
        console.log(result); // "Stuff worked!"
    }, function(err) {
        console.log(err); // Error: "It broke"
    });


    /*xmlRequest("test",1,2,3, function(aph_price){
        let speechText = "";
        if(aph_price){
            speechText = "Price found, it is £"+aph_price;
        }else{
            speechText = "Can't find any prices";
        }

        console.log(speechText);
    });*/
    //console.log(aph_price);
}

function xmlRequestPromise(arrival_date, duration, arrival_time, depart_time){

    return new Promise(function(resolve, reject) {
        var request = require("request");
//var utf8 = require('utf8');

        var xml_request_string = '<?xml version="1.0" encoding="UTF-8"?> <API_Request System="APH" Version="1.0" Product="CarPark" Customer="X" Session="000000003" RequestCode="11"> <Agent> <ABTANumber>APH</ABTANumber> <Password></Password> <Initials>APH</Initials> </Agent> <Itinerary> <ArrivalDate>30May19</ArrivalDate> <DepartDate>07Jun19</DepartDate> <ArrivalTime>1200</ArrivalTime> <DepartTime>1200</DepartTime> <Location>LGW</Location> </Itinerary> </API_Request>';

        request.post({
                url: "https://agents.aph.com/xmlapi/aphxml.ASP",
                port: 443,
                method: "POST",
                headers: {
                    'Content-Type': 'application/xml',
                },
                formData: {
                    "request": xml_request_string
                }

            },
            function (error, response, body) {

                var aph_price = 0;
                if (response.statusCode == 200) {

                    var parseString = require('xml2js').parseString;
                    parseString(body, function (err, result) {
                        //console.dir(result);
                        //console.log(result.API_Reply.$.System);
                        //console.log(util.inspect(result, false, null))
                        var car_park_list = result.API_Reply.CarPark;
                        //console.log(car_park_list);

                        car_park_list.forEach(function (element) {

                            //var car_park_code = element.CarParkCode[0];

                            if (element.ProductCode[0] == "LGW1") {
                                //console.log(element.TotalPrice[0]);
                                aph_price = element.TotalPrice[0];
                            }

                            //console.log(element.CarParkCode[0]);
                        });
                    });

                    if (aph_price) {
                        resolve(aph_price);

                    } else {
                        reject(false);
                        //console.log("Can't find price");
                    }

                } else {

                    reject(false);

                    //console.log(response.statusCode);
                    //console.log(body);
                    //console.log(error);
                }
            });

        //return false;
    });

}

function xmlRequestCallBack(arrival_date, duration, arrival_time, depart_time, callback){
    var request = require("request");
//var utf8 = require('utf8');

    var xml_request_string = '<?xml version="1.0" encoding="UTF-8"?> <API_Request System="APH" Version="1.0" Product="CarPark" Customer="X" Session="000000003" RequestCode="11"> <Agent> <ABTANumber>APH</ABTANumber> <Password></Password> <Initials>APH</Initials> </Agent> <Itinerary> <ArrivalDate>30May19</ArrivalDate> <DepartDate>07Jun19</DepartDate> <ArrivalTime>1200</ArrivalTime> <DepartTime>1200</DepartTime> <Location>LGW</Location> </Itinerary> </API_Request>';

    request.post({
            url:"https://agents.aph.com/xmlapi/aphxml.ASP",
            port: 443,
            method:"POST",
            headers:{
                'Content-Type': 'application/xml',
            },
            formData : {
                "request" : xml_request_string
            }

        },
        function(error, response, body){

            var aph_price = 0;
            if(response.statusCode == 200){

                var parseString = require('xml2js').parseString;
                parseString(body, function (err, result) {
                    //console.dir(result);
                    //console.log(result.API_Reply.$.System);
                    //console.log(util.inspect(result, false, null))
                    var car_park_list = result.API_Reply.CarPark;
                    //console.log(car_park_list);

                    car_park_list.forEach(function(element) {

                        //var car_park_code = element.CarParkCode[0];

                        if(element.ProductCode[0] == "LGW1"){
                            //console.log(element.TotalPrice[0]);
                            aph_price = element.TotalPrice[0];
                        }

                        //console.log(element.CarParkCode[0]);
                    });
                });

                if(aph_price){
                    return callback(aph_price);

                }else{
                    return callback(false);
                    //console.log("Can't find price");
                }

            }else{

                return callback(false);

                //console.log(response.statusCode);
                //console.log(body);
                //console.log(error);
            }
        });

    return false;

}

function xmlRequestTwo(){

    var xml_request_string = '<?xml version="1.0" encoding="UTF-8"?> <API_Request System="APH" Version="1.0" Product="CarPark" Customer="X" Session="000000003" RequestCode="11"> <Agent> <ABTANumber>APH</ABTANumber> <Password></Password> <Initials>APH</Initials> </Agent> <Itinerary> <ArrivalDate>30May19</ArrivalDate> <DepartDate>07Jun19</DepartDate> <ArrivalTime>1200</ArrivalTime> <DepartTime>1200</DepartTime> <Location>LGW</Location> </Itinerary> </API_Request>';



    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'https://agents.aph.com/xmlapi/aphxml.ASP', true);

//Send the proper header information along with the request
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {

            console.log("success");
            // Request finished. Do processing here.
        }else{
            console.log("fail");
        }
    };
    xhr.send("request="+xml_request_string);
// xhr.send(new Int8Array());
// xhr.send(document);
}

function xmlRequestThree(){
    var xml_request_string = '<?xml version="1.0" encoding="UTF-8"?> <API_Request System="APH" Version="1.0" Product="CarPark" Customer="X" Session="000000003" RequestCode="11"> <Agent> <ABTANumber>APH</ABTANumber> <Password></Password> <Initials>APH</Initials> </Agent> <Itinerary> <ArrivalDate>30May19</ArrivalDate> <DepartDate>07Jun19</DepartDate> <ArrivalTime>1200</ArrivalTime> <DepartTime>1200</DepartTime> <Location>LGW</Location> </Itinerary> </API_Request>';
    //var xml_request_string = '<?xml version=\\"1.0\\" encoding=\\"UTF-8\\"?> <API_Request System=\\"APH\\" Version=\\"1.0\\" Product=\\"CarPark\\" Customer=\\"X\\" Session=\\"000000003\\" RequestCode=\\"11\\"> <Agent> <ABTANumber>APH</ABTANumber> <Password></Password> <Initials>APH</Initials> </Agent> <Itinerary> <ArrivalDate>30May19</ArrivalDate> <DepartDate>07Jun19</DepartDate> <ArrivalTime>1200</ArrivalTime> <DepartTime>1200</DepartTime> <Location>LGW</Location> </Itinerary> </API_Request>';


    var https = require("https");
    var options = {
        hostname: 'agents.aph.com',
        port: 443,
        path: '/xmlapi/aphxml.ASP',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',

        }
    };
    // request object
    var req = https.request(options, function (res) {
        var result = '';
        res.on('data', function (chunk) {
            result += chunk;
        });
        res.on('end', function () {
            //console.log(result);

            if(res.statusCode == 200){

                var aph_price = null;
                var parseString = require('xml2js').parseString;
                parseString(result, function (err, xml) {
                    //console.dir(result);
                    //console.log(result.API_Reply.$.System);
                    //console.log(util.inspect(result, false, null))
                    var car_park_list = xml.API_Reply.CarPark;
                    //console.log(car_park_list);

                    car_park_list.forEach(function(element) {

                        //var car_park_code = element.CarParkCode[0];

                        if(element.ProductCode[0] == "LGW1"){
                            //console.log(element.TotalPrice[0]);
                            aph_price = element.TotalPrice[0];
                        }

                        //console.log(element.CarParkCode[0]);
                    });
                });

                if(aph_price){
                    console.log(aph_price);
                }else{
                    //Error
                    console.log("Can't find price");
                }

            }else{
                //Error
                console.log(res.statusCode);
            }

        });


        res.on('error', function (err) {
            //Error
            console.log(err);
        });

        //console.log(res.statusCode);
    });

// req error
    req.on('error', function (err) {
        //Error
        console.log(err);
    });

//send request with the postData form
    req.write("request="+xml_request_string);
    req.end();
}

exports.handler = index;