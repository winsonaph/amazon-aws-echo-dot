// This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
// Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
// session persistence, api calls, and more.
const Alexa = require('ask-sdk-core');

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    handle(handlerInput) {
        const speechText = 'Welcome to Airport Parking and Hotels.';
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt("You can say quote parking to find prices for your booking at APH.")
            .withSimpleCard("APH: Launch", speechText)
            .getResponse();
    }
};

const QuoteParkingIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'QuoteParkingIntent';
    },
    async handle(handlerInput) {

        //Get slots/variables from user requests
        const slot_list = handlerInput.requestEnvelope.request.intent.slots;

        const arrival_datetime = slot_list.arrival_date.value + " " + slot_list.arrival_time.value;
        const departure_datetime = slot_list.departure_date.value + " " + slot_list.departure_time.value;

        const arrival_date = formatDateXML(arrival_datetime);
        const arrival_time = formatTimeXML(arrival_datetime);
        const departure_date = formatDateXML(departure_datetime);
        const departure_time = formatTimeXML(departure_datetime);

        //Send XML Request
        const response = await xmlRequest(arrival_date, departure_date, arrival_time, departure_time);

        let speechText = "";
        if(response){
            speechText = "Your car park price is £"+response+".";
        }else{
            speechText = "Unfortunately we could not find any prices.";
        }

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt('Do you want to do another quote?')
            .withSimpleCard("APH: Quote Parking", speechText)
            .getResponse();

        /*let speechText = "placeholder";
        response.then(function(result) {
            // "Stuff worked!"
            speechText = "Your car park price is £"+response;
        }, function(err) {
            // Error: "It broke"
             speechText = "Unfortunately we could not find any prices";
        });

        return handlerInput.responseBuilder
                .speak(speechText)
                //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
                .getResponse();*/
    }
};

const HelpIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speechText = 'You can say quote parking to me to find prices for APH parking.';

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }

};

/*async handle(handlerInput) {
        const { requestEnvelope, serviceClientFactory, responseBuilder } = handlerInput;
        const token = requestEnvelope.context.System.user.permissions &&
            requestEnvelope.context.System.user.permissions.consentToken;

        if (!token) {
            return handlerInput.responseBuilder
                .speak('Please Provide Permissions!')
                .withAskForPermissionsConsentCard(['alexa::profile:email:read'])
                .getResponse();
        }

        let {deviceId} = requestEnvelope.context.System.device;
        const upsServiceClient = serviceClientFactory.getUpsServiceClient();
        const email = await upsServiceClient.getProfileEmail();

        let speechText = `Hello! Your email is ${email}`;

        return handlerInput.responseBuilder
            .speak(speechText)
            .withSimpleCard('Hello World', speechText)
            .getResponse();

    },*/


const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
                || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speechText = 'Goodbye, thank you for using Airport Parking and Hotels!';
        return handlerInput.responseBuilder
            .speak(speechText)
            .getResponse();
    }
};


/*const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse();
    }
};
*/


const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        if(null!=handlerInput.requestEnvelope.request.error) {
            console.log(JSON.stringify(handlerInput.requestEnvelope.request.error));
        }


        return handlerInput.responseBuilder.getResponse();
    },
};

// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = handlerInput.requestEnvelope.request.intent.name;
        const speechText = `You just triggered ${intentName}`;

        return handlerInput.responseBuilder
            .speak(speechText)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`~~~~ Error handled: ${error.message}`);
        const speechText = `Sorry, I couldn't understand what you said. Please try again.`;

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};

function xmlRequest(arrival_date, depart_date, arrival_time, depart_time){

    return new Promise(function(resolve, reject) {
        var request = require("request");

        var xml_request_string = '<?xml version="1.0" encoding="UTF-8"?> <API_Request System="APH" Version="1.0" Product="CarPark" Customer="X" Session="000000003" RequestCode="11"> <Agent> <ABTANumber>APH</ABTANumber> <Password></Password> <Initials>APH</Initials> </Agent> <Itinerary> <ArrivalDate>30May19</ArrivalDate> <DepartDate>07Jun19</DepartDate> <ArrivalTime>1200</ArrivalTime> <DepartTime>1200</DepartTime> <Location>LGW</Location> </Itinerary> </API_Request>';
        //var xml_request_string = '<?xml version="1.0" encoding="UTF-8"?> <API_Request System="APH" Version="1.0" Product="CarPark" Customer="X" Session="000000003" RequestCode="11"> <Agent> <ABTANumber>APH</ABTANumber> <Password></Password> <Initials>APH</Initials> </Agent> <Itinerary> <ArrivalDate>'+arrival_date+'</ArrivalDate> <DepartDate>'+depart_date+'</DepartDate> <ArrivalTime>'+arrival_time+'</ArrivalTime> <DepartTime>'+depart_time+'</DepartTime> <Location>LGW</Location> </Itinerary> </API_Request>';

        request.post({
                url: "https://agents.aph.com/xmlapi/aphxml.ASP",
                port: 443,
                method: "POST",
                headers: {
                    'Content-Type': 'application/xml',
                },
                formData: {
                    "request": xml_request_string
                }

            },
            function (error, response, body) {

                var aph_price = 0;
                if (response.statusCode == 200) {

                    var parseString = require('xml2js').parseString;
                    parseString(body, function (err, result) {

                        var car_park_list = result.API_Reply.CarPark;

                        car_park_list.forEach(function (element) {
                            if (element.ProductCode[0] == "LGW1") {
                                aph_price = element.TotalPrice[0];
                            }
                        });
                    });

                    if (aph_price) {
                        resolve(aph_price);
                    } else {
                        resolve(aph_price);
                    }
                } else {
                    resolve(aph_price);
                }
            });
    });
}

function formatDateXML(datetime){
    const date = new Date(datetime);
    return date.getDate()+ date.toLocaleString('en-gb', { month: 'long' }) + (date.getFullYear() - 2000);
}

function formatTimeXML(datetime){
    const date = new Date(datetime);
    return date.getHours()+""+date.getMinutes();
}


// This handler acts as the entry point for your skill, routing all request and response
// payloads to the handlers above. Make sure any new handlers or interceptors you've
// defined are included below. The order matters - they're processed top to bottom.
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        QuoteParkingIntentHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        SessionEndedRequestHandler,
        IntentReflectorHandler) // make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
    .addErrorHandlers(
        ErrorHandler)
    .withApiClient(new Alexa.DefaultApiClient())
    .lambda();
