// This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
// Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
// session persistence, api calls, and more.
const Alexa = require('ask-sdk-core');
const Helper = require('./helper');
//const sgMail = require('@sendgrid/mail');
//const HandleBars = require('handlebars');
//const FS = require('fs'); //Filesystem
const RequestLib = require("request");

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    handle(handlerInput) {
        const speechText = 'Welcome to Airport Parking and Hotels, please say quote parking to find prices at APH.';
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt("You can say quote parking to find prices for your booking at APH.")
            .withSimpleCard("APH: Welcome", speechText)
            .getResponse();
    }
};

const TestIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'TestIntent';
    },
    async handle(handlerInput) {

        //Use handle bars and FS library to create email
        /*let content = FS.readFileSync("./email-quote.html","utf-8");
        let json_data = {QUOTE_REFERENCE: "QIANDA", PRICE: 70.11, GLOBAL_BASE_URL: "https://www.aph.com"};
        let template = HandleBars.compile(content);
        let result = template(json_data);

        sgMail.setApiKey('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');
        const msg = {
            to: 'winsonlau@aph.com',
            from: 'no-reply@aph.com',
            subject: 'Your APH booking quote',
            text: 'Via Alexa Voice Chat',
            html: result
        };
        sgMail.send(msg);*/

        //https://www.aph.com/services/quote/generateToken
        //https://www.aph.com/services/savequote/submit
        //let save_quote_results = await saveQuote("wahatew");
        //console.log(save_quote_results);

        // const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        // sessionAttributes.apple = "yup";
        // handlerInput.attributesManager.setSessionAttributes(sessionAttributes);
        // const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        // console.log(sessionAttributes.apple);

        let save_quote_results = "";
        let send_quote_results = await getQuoteGUID("25 May 2019", "1830", "29 May 2019","1530");
        if(send_quote_results.quote_guid){
            save_quote_results = await saveQuote(send_quote_results.quote_guid,"winsonlau@aph.com");
            console.log(save_quote_results);
        }

        const speechText = "We have successfully sent your email.";
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt("Meh.")
            .withSimpleCard("APH: Email Sent", speechText)
            .getResponse();
    }
};

function getQuoteGUID(arrival_date, arrival_time, return_date, return_time){

    return new Promise(function(resolve, reject) {

        var options = {
            uri: 'https://www.aph.com/services/quote/generateToken',
            method: 'POST',
            form: {
                agent_code: "APH",
                voucher_code: "",
                site_code: "LGW1",
                search_type: "pa",
                airport_code: "LGW",
                airport_terminal: "any",
                arrival_date: arrival_date,
                arrival_time: arrival_time,
                return_date: return_date,
                return_time: return_time,
                number_vehicles: 1,
                trg_page: "SITE_DETAILS",
                trg_product_type: "PARKING",
                trg_airport_code: "LGW"
            },
            json: true
        };

        RequestLib.post(
            options,
            function (error, response, body) {
                if (!error) {
                    resolve(body);
                }else{
                    resolve(null);
                }
                console.log("Get Quote:");
                console.log(body);
            }
        );
    });

}

function saveQuote(guid, email){

    return new Promise(function(resolve, reject) {

        RequestLib.post(

            {
                headers: {'Content-Type' : 'application/x-www-form-urlencoded','X-Requested-With':'XMLHttpRequest'},
                url:     'https://www.aph.com/services/savequote/submit',
                body:    "guid="+guid+"&email_address="+email
            },
            function (error, response, body) {
                // console.log("GUID: "+guid);
                // console.log("Save Quote:");
                // console.log(error);
                // console.log(body);
                // console.log(response);
                if (!error) {
                    resolve(body);
                    //console.log("successful");
                }else{
                    resolve(null);
                    //console.log("fail");
                }
            }
        );
    });

}

const QuoteParkingIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'QuoteParkingIntent';
    },
    async handle(handlerInput) {

        const request = handlerInput.requestEnvelope.request;

        //If user says yes to correct quote parking details
        if (request.intent.confirmationStatus === "CONFIRMED") {
            //Get slots/variables from user requests
            const slot_list = handlerInput.requestEnvelope.request.intent.slots;

            const arrival_datetime = slot_list.arrival_date.value + " " + slot_list.arrival_time.value;
            const departure_datetime = slot_list.departure_date.value + " " + slot_list.departure_time.value;

            const arrival_date = Helper.formatDateXML(arrival_datetime);
            const arrival_time = Helper.formatTimeXML(arrival_datetime);
            const departure_date = Helper.formatDateXML(departure_datetime);
            const departure_time = Helper.formatTimeXML(departure_datetime);

            //Send XML Request
            const response = await xmlRequest(arrival_date, departure_date, arrival_time, departure_time);

            let speech_text = "";
            let reprompt_text = "";
            if(response){
                speech_text = "Your car park price is £"+response+", would you like to send the quote to your email?";
                reprompt_text = "Would you like to send the quote to your email?";

                //Set sessions
                const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
                sessionAttributes.arrival_date = Helper.formatDateXML(arrival_datetime,true);
                sessionAttributes.arrival_time = arrival_time;
                sessionAttributes.departure_date = Helper.formatDateXML(departure_datetime,true);
                sessionAttributes.departure_time = departure_time;
                handlerInput.attributesManager.setSessionAttributes(sessionAttributes);

                return handlerInput.responseBuilder
                    .speak(speech_text)
                    .addDelegateDirective(
                        {
                            name: 'SendQuoteIntent',
                            confirmationStatus: 'NONE',
                            slots: {
                                "price": {
                                    "name": "price",
                                    "value": response,
                                    "confirmationStatus": "NONE"
                                }
                            }
                        })
                    .reprompt(reprompt_text)
                    .withSimpleCard("APH: Quote Parking", speech_text)
                    .getResponse();

            }else{
                speech_text = "Unfortunately we could not find any prices, please say quote parking if you would like to try again.";
                reprompt_text = "Please say quote parking if you would like to try again.";

                return handlerInput.responseBuilder
                    .speak(speech_text)
                    .reprompt(reprompt_text)
                    .withSimpleCard("APH: Quote Parking", speech_text)
                    .getResponse();
            }
            //console.log(speechText);



            /*"quote_reference": {
                "name": "quote_reference",
                    "value": "ABC20",
                    "resolutions": {},
                "confirmationStatus": "NONE"
            }*/

        }else{

            const speech_text = "I'm sorry, please say quote parking and we can try again.";

            return handlerInput.responseBuilder
                .speak(speech_text)
                .reprompt('Please say quote parking and we can try again.')
                .withSimpleCard("APH: Quote Parking", speech_text)
                .getResponse();

            /*return handlerInput.responseBuilder
                .speak("I'm sorry, lets try again.")
                .addDelegateDirective(
                    {
                        name: 'QuoteParkingIntent',
                        confirmationStatus: 'CONFIRMED',
                        slots: {}
                    })
                //.reprompt('Do you want to do another quote?')
                .getResponse();*/

        }
    }
};

const SendQuoteIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'SendQuoteIntent';
    },
    async handle(handlerInput) {

        const request = handlerInput.requestEnvelope.request;
        //const responseBuilder = handlerInput.responseBuilder;
        //let sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

        // delegate to Alexa to collect all the required slots
        //const currentIntent = request.intent;
        let speechText = '';
        if (request.intent.confirmationStatus === "CONFIRMED") {
            //speechText = 'We have sent you a message to your email.';

            const { requestEnvelope, serviceClientFactory, responseBuilder } = handlerInput;
            const token = requestEnvelope.context.System.user.permissions &&
                requestEnvelope.context.System.user.permissions.consentToken;

            //Only return error to user because we don't have email
            if (token) {

                let {deviceId} = requestEnvelope.context.System.device;
                const upsServiceClient = serviceClientFactory.getUpsServiceClient();
                const email = await upsServiceClient.getProfileEmail();
                const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
                console.log(sessionAttributes);
                console.log(sessionAttributes.arrival_date);

                //Save quote
                let save_quote_results = "";
                let send_quote_results = await getQuoteGUID(sessionAttributes.arrival_date, sessionAttributes.arrival_time, sessionAttributes.departure_date,sessionAttributes.departure_time);
                if(send_quote_results.quote_guid){
                    save_quote_results = await saveQuote(send_quote_results.quote_guid, email);
                    console.log(save_quote_results);

                    speechText = 'We have successfully sent an email to '+email+", say help if you need anything else.";
                    return handlerInput.responseBuilder
                        .speak(speechText)
                        .reprompt("Say help if you need anything else.")
                        .withSimpleCard('APH: Email Sent', speechText)
                        .getResponse();
                }else{
                    speechText = 'Unfortunately there was an error in sending your quote, you can say send last quote to try again';
                    return handlerInput.responseBuilder
                        .speak(speechText)
                        .reprompt("You can say email last quote to try again.")
                        .withSimpleCard('APH: Email Sent', speechText)
                        .getResponse();
                }

                //Get slots/variables from user requests
                /*const slot_list = request.intent.slots;
                const price = slot_list.price.value;

                let {deviceId} = requestEnvelope.context.System.device;
                const upsServiceClient = serviceClientFactory.getUpsServiceClient();
                const email = await upsServiceClient.getProfileEmail();

                //Use handle bars and FS library to create email
                let content = FS.readFileSync("./email-quote.html","utf-8");
                let json_data = {QUOTE_REFERENCE: "QIANDA", PRICE: price, GLOBAL_BASE_URL: "https://www.aph.com"};
                let template = HandleBars.compile(content);
                let result = template(json_data);

                sgMail.setApiKey('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');
                const msg = {
                    to: email,
                    from: 'no-reply@aph.com',
                    subject: 'Your APH booking quote',
                    text: 'Via Alexa Voice Chat',
                    html: result
                };
                sgMail.send(msg);*/


            }else{
                return handlerInput.responseBuilder
                    .speak('Unfortunately we do not have access to your email, please provide the permission on your companion app.')
                    .reprompt("You can say email last quote to try again.")
                    .withAskForPermissionsConsentCard(['alexa::profile:email:read'])
                    .getResponse();
            }

        }else{
            speechText = 'No worries, please say quote parking if you need another quote.';

            return handlerInput.responseBuilder
                .speak(speechText)
                .reprompt(speechText)
                .getResponse();
        }
    }
};

const SendLastQuoteHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'SendLastQuoteIntent';
    },
    async handle(handlerInput) {
        const { requestEnvelope, serviceClientFactory, responseBuilder } = handlerInput;
        const token = requestEnvelope.context.System.user.permissions &&
            requestEnvelope.context.System.user.permissions.consentToken;

        //Only return error to user because we don't have email
        if (token) {

            let {deviceId} = requestEnvelope.context.System.device;
            const upsServiceClient = serviceClientFactory.getUpsServiceClient();
            const email = await upsServiceClient.getProfileEmail();
            const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

            if(sessionAttributes.arrival_date) {

                //Save quote
                let save_quote_results = "";
                let send_quote_results = await getQuoteGUID(sessionAttributes.arrival_date, sessionAttributes.arrival_time, sessionAttributes.departure_date, sessionAttributes.departure_time);
                if (send_quote_results.quote_guid) {
                    save_quote_results = await saveQuote(send_quote_results.quote_guid, email);

                    speechText = 'We have successfully sent an email to ' + email + ", say help if you need anything else.";
                    return handlerInput.responseBuilder
                        .speak(speechText)
                        .reprompt("Say help if you need anything else.")
                        .withSimpleCard('APH: Email Sent', speechText)
                        .getResponse();
                } else {
                    speechText = 'Unfortunately there was an error in sending your quote';
                    return handlerInput.responseBuilder
                        .speak(speechText)
                        .reprompt("Say help if you need anything else.")
                        .withSimpleCard('APH: Email Sent', speechText)
                        .getResponse();
                }
            }else{
                speechText = 'We could not find any previous quotes, please say quote parking to find prices.';
                return handlerInput.responseBuilder
                    .speak(speechText)
                    .reprompt("Say help if you need anything else.")
                    .withSimpleCard('APH: Email Sent', speechText)
                    .getResponse();
            }
        }else{
            return handlerInput.responseBuilder
                .speak('Unfortunately we do not have access to your email, please provide the permission on your companion app.')
                .reprompt("You can say email last quote to try again.")
                .withAskForPermissionsConsentCard(['alexa::profile:email:read'])
                .getResponse();
        }
    }
};




const HelpIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speechText = 'You can say quote parking to me to find prices for APH parking.';

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }

};


/*const YesHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
            (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.YesIntent' ||
                handlerInput.requestEnvelope.request.intent.name === 'GetRandomFactIntent');
    },
    handle(handlerInput) {
        console.log('In YesHandler');

        const speakOutput = 'Sent email';


        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    },
};*/

/*async handle(handlerInput) {
        const { requestEnvelope, serviceClientFactory, responseBuilder } = handlerInput;
        const token = requestEnvelope.context.System.user.permissions &&
            requestEnvelope.context.System.user.permissions.consentToken;

        if (!token) {
            return handlerInput.responseBuilder
                .speak('Please Provide Permissions!')
                .withAskForPermissionsConsentCard(['alexa::profile:email:read'])
                .getResponse();
        }

        let {deviceId} = requestEnvelope.context.System.device;
        const upsServiceClient = serviceClientFactory.getUpsServiceClient();
        const email = await upsServiceClient.getProfileEmail();

        let speechText = `Hello! Your email is ${email}`;

        return handlerInput.responseBuilder
            .speak(speechText)
            .withSimpleCard('Hello World', speechText)
            .getResponse();

    },*/


const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
                || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speechText = 'Goodbye, thank you for using Airport Parking and Hotels!';
        return handlerInput.responseBuilder
            .speak(speechText)
            .getResponse();
    }
};


const FallbackHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
            handlerInput.requestEnvelope.request.intent.name === 'AMAZON.FallbackIntent';
    },
    handle(handlerInput) {
        console.log('IN FallbackHandler');
        return handlerInput.responseBuilder
            .speak('Sorry, I didn\'t understand what you meant. Please try again.')
            .reprompt('Sorry, I didn\'t understand what you meant. Please try again.')
            .getResponse();
    },
};

/*const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse();
    }
};
*/


const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        if(null!=handlerInput.requestEnvelope.request.error) {
            console.log(JSON.stringify(handlerInput.requestEnvelope.request.error));
        }


        return handlerInput.responseBuilder.getResponse();
    },
};

// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = handlerInput.requestEnvelope.request.intent.name;
        const speechText = `You just triggered ${intentName}`;

        return handlerInput.responseBuilder
            .speak(speechText)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`~~~~ Error handled: ${error.message}`);
        const speechText = `Sorry, there was an unforeseen error. Please try again.`;

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};

function xmlRequest(arrival_date, depart_date, arrival_time, depart_time){

    return new Promise(function(resolve, reject) {
        var request = require("request");

        //var xml_request_string = '<?xml version="1.0" encoding="UTF-8"?> <API_Request System="APH" Version="1.0" Product="CarPark" Customer="X" Session="000000003" RequestCode="11"> <Agent> <ABTANumber>APH</ABTANumber> <Password></Password> <Initials>APH</Initials> </Agent> <Itinerary> <ArrivalDate>30May19</ArrivalDate> <DepartDate>07Jun19</DepartDate> <ArrivalTime>1200</ArrivalTime> <DepartTime>1200</DepartTime> <Location>LGW</Location> </Itinerary> </API_Request>';
        var xml_request_string = '<?xml version="1.0" encoding="UTF-8"?> <API_Request System="APH" Version="1.0" Product="CarPark" Customer="X" Session="000000003" RequestCode="11"> <Agent> <ABTANumber>APH</ABTANumber> <Password></Password> <Initials>APH</Initials> </Agent> <Itinerary> <ArrivalDate>'+arrival_date+'</ArrivalDate> <DepartDate>'+depart_date+'</DepartDate> <ArrivalTime>'+arrival_time+'</ArrivalTime> <DepartTime>'+depart_time+'</DepartTime> <Location>LGW</Location> </Itinerary> </API_Request>';

        request.post({
                url: "https://agents.aph.com/xmlapi/aphxml.ASP",
                port: 443,
                method: "POST",
                headers: {
                    'Content-Type': 'application/xml',
                },
                formData: {
                    "request": xml_request_string
                }

            },
            function (error, response, body) {

                var aph_price = 0;
                if (response.statusCode == 200) {

                    var parseString = require('xml2js').parseString;
                    parseString(body, function (err, result) {

                        var car_park_list = result.API_Reply.CarPark;

                        if(car_park_list) {
                            car_park_list.forEach(function (element) {
                                if (element.ProductCode[0] == "LGW1") {
                                    aph_price = element.TotalPrice[0];
                                }
                            });
                        }
                    });

                    resolve(aph_price);

                    /*if (aph_price) {
                        resolve(aph_price);
                    } else {
                        resolve(aph_price);
                    }*/
                } else {
                    resolve(aph_price);
                }
            });
    });
}

Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
};

// This handler acts as the entry point for your skill, routing all request and response
// payloads to the handlers above. Make sure any new handlers or interceptors you've
// defined are included below. The order matters - they're processed top to bottom.
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        TestIntentHandler,
        QuoteParkingIntentHandler,
        SendQuoteIntentHandler,
        SendLastQuoteHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        SessionEndedRequestHandler,
        FallbackHandler,
        IntentReflectorHandler) // make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
    .addErrorHandlers(
        ErrorHandler)
    .withApiClient(new Alexa.DefaultApiClient())
    .lambda();
