module.exports = {
    formatDateXML(datetime, aph_service){
        const date = new Date(datetime);
        if(aph_service){
            return date.getDate().pad(2)+ " "+date.toLocaleString('en-gb', { month: 'short' }) + " "+(date.getFullYear() - 2000).pad(2);
        }else{
            return date.getDate().pad(2)+ date.toLocaleString('en-gb', { month: 'short' }) + (date.getFullYear() - 2000).pad(2);
        }
    },
    formatTimeXML(datetime){
        const date = new Date(datetime);
        return date.getHours().pad(2)+""+date.getMinutes().pad(2);
    }
};